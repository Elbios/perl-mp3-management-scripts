use strict;
use warnings;
use File::Copy;

open (FILE, $ARGV[0]) or die "Cant open $!";

while (<FILE>)
{
	chomp;
	my $from_dir = qq/"$_"/;
	my $to_dir = substr($_, 0, 1) .':/music';
	$to_dir = qq/"$to_dir"/;
	$from_dir =~ s-/-\\-g;
	$to_dir =~ s-/-\\-g;
	print qx/move $from_dir $to_dir/;
}
