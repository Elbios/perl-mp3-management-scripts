use strict;
use warnings;
use File::Find;

my @dirs;
push @dirs, $ARGV[0];
my @audio_extensions = ("mp3", "flac", "m4a", "ape", "mpc", "ogg", "wav", "aac");
my %audio_extensions_hash = map { $_ => 1 } @audio_extensions;

my @all_extensions;

#foreach(@dirs){
#	print $_ . "\n";
#}



find(		{wanted =>
		sub{
			my $extension;
			if (-f $_)
			{
				$extension = (split /\./, $_)[-1];
				if ((defined $extension) and ($extension =~ /\w{1,4}/))
				{					
					if (not exists $audio_extensions_hash{lc $extension})
					{
						push @all_extensions, $extension;
					}
				}
			}
		}
		}
		, @dirs
);

my @sorted_exts = sort @all_extensions;

foreach(@sorted_exts){
	print $_ . "\n";
}