use strict;
use warnings;
use File::Find;

my @audio_extensions = ("mp3", "flac", "m4a", "ape", "mpc", "ogg", "wav", "aac");
my %audio_extensions_hash = map { $_ => 1 } @audio_extensions;

my $root = $ARGV[0]; #root dir passed as arg

#find all music directories directly on the root dir
#count all music files and compare to threshold, if majority is audio then its a music folder

chdir($root) or die "Cannot chdir to given path ($!)";
opendir (ROOT, '.') or die $!;
my @dirs = grep {-d "$root/$_" && !/^\.{1,2}$/} readdir ROOT;

#foreach(@dirs){
#	print $_ . "\n";
#}

my $max_depth = 1;
my $audio_files_cnt = 0;
my $all_files_cnt = 0;
my @non_music_folders;

find({ 
		preprocess =>
		sub{
			#must return list of files to be processed (passed to wanted)
			my $depth = $File::Find::dir =~ tr!/!!;
			return @_ if $depth < $max_depth; #count slashes to know depth, explore only up to 3 subdirs
			return grep {not -d} @_ if $depth == $max_depth;
			return;
		},
		wanted =>
		sub{
			my $extension;
			if (-f $_)
			{
				$extension = (split /\./, $_)[-1];
				if ((defined $extension) and ($extension =~ /\w{1,4}/))
				{
					$all_files_cnt += 1;
					if (exists $audio_extensions_hash{lc $extension})
					{
						$audio_files_cnt += 1;
					}
				}
			}
		},
		postprocess =>
		sub{
			if (($File::Find::dir =~ tr!/!!) == 0)
			{
				return if ($all_files_cnt == 0);
				if (($audio_files_cnt/$all_files_cnt) < 0.35)
				{
					push @non_music_folders, ($root . $File::Find::dir) ;
				}
				$audio_files_cnt = 0;
				$all_files_cnt = 0; #reset counter for next folder in root
			}
		},
		no_chdir => 1
	},
	@dirs
);

foreach(@non_music_folders){
	print $_ . "\n";
}